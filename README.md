# D-MATH Oral Exam Form

This LaTeX code will generate door signs and pre-printed forms for taking minutes for oral exams. 

[**Here is an example**](https://gitlab.math.ethz.ch/teaching/oral-exam-form/-/blob/main/oral_exam_form.pdf) of the documents (door signs and pre-printed forms for all students) generated with this code:

Door sign            |  Form
:-------------------------:|:-------------------------:
![](https://gitlab.math.ethz.ch/teaching/oral-exam-form/-/wikis/uploads/caeddbe59d2f1b9cfba0be9d395341de/oral_exam_form.jpg)  |  ![](https://gitlab.math.ethz.ch/teaching/oral-exam-form/-/wikis/uploads/341994776b9d48545630b55077e1761b/oral_exam_form_2.jpg)

## Using the code

1. [Download](https://gitlab.math.ethz.ch/teaching/oral-exam-form/-/archive/main/oral-exam-form-main.zip) or clone the repository. 
2. Download the list of enrolled students as an Excel file from eDoz: For this step you must possess the necessary rights. Alternatively you can ask the lecturer to eMail you the downloaded file.
![](https://gitlab.math.ethz.ch/teaching/oral-exam-form/-/wikis/uploads/07051c53d6ab17f1b4afbaeef884b5d3/eDoz.png)
3. Use [this tool](https://isg.math.ethz.ch/make-anonym-csv/) to covert the list to the `registration.csv` file, containing student data.
4. Input the exam specific data into the `oral_exam_form.tex`: Exam name, number, dates, times, etc. (see quoted code below, or follow the instructions in the TeX file). 
5. Compile and print (single sided, no staple).



## Filling exam specific information (Step 4 above)

To generate the door signs, you will have to fill the following portions of the code with the necessary data. The example provided is self-explanatory. None the less, here is what needs to be provided.

#### Course data from the course catalogue (VVZ)
> % Name of course 
> 
> \def \coursename {Probability Theory}
> 
> % Course Number
> 
> \newcommand{\coursenumber}{401-3601-00S}
> 
> % Examiner name
> 
> \newcommand{\examiner}{Prof. Dr. A.-S. Sznitman}
> 
> % Dept
> 
> \newcommand{\dept}{D-MATH}

#### Dates and rooms from eDoz
Each three-line code block generates an additional door sign for the specific date and exam room. 
> % Rooms and Dates
> 
> 	\DTLnewdb{rooms} % Don't change this line
> 
> %%
> 
> % A door signs using the following three commands for each
> 
> %
> 
> %\DTLnewrow{rooms}
> 
> %\DTLnewdbentry{rooms}{Room}{HG G 43}
> 
> %\DTLnewdbentry{rooms}{Date}{24. Aug. 2021}
> 
> %
> 
> %%
> 
> % Room for first day of exams
> 
> \DTLnewrow{rooms}
> 
> \DTLnewdbentry{rooms}{Room}{HG G 43}
> 
> \DTLnewdbentry{rooms}{Date}{24. Aug. 2021}
> 
> % Room for second day of exams
> 
> \DTLnewrow{rooms}
> 
> \DTLnewdbentry{rooms}{Room}{HG G 43}
> 
> \DTLnewdbentry{rooms}{Date}{25. Aug. 2021}

#### Names of the assistants taking minutes
Assistants can then circle or underline their name to indicate they took the minutes. This line can be left blank.
 
> % Assistants' names (all assistants taking minutes): Assistants can then circle or underline their name to indicate they were present at the exam.
> 
> \newcommand{\assistant}{Name 1, Name 2, Name 3, ...}

